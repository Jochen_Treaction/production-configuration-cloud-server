# README #
INSTALLATION (https://symfony.com/doc/current/deployment.html)

.env for production
Run "composer dump-env prod" to compile .env files for production use (requires symfony/flex >=1.2).
$> composer require symfony/dotenv v4.3.0
$> composer dump-env prod


Install/Update your Vendors¶
Your vendors can be updated before transferring your source code (i.e. update the vendor/ directory, then transfer that with your source code) or afterwards on the server. Either way, update your vendors as you # normally do:
$> composer install --no-dev --optimize-autoloader

Make sure you clear and warm-up your Symfony cache:
$>  APP_ENV=prod APP_DEBUG=0 php bin/console cache:clear
